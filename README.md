### Projekt-Readme

#### Übersicht

In diesem Projekt wird eine lokale Kubernetes-Umgebung mit Vagrant, Ansible und Kubernetes eingerichtet. Vagrant dient zur Bereitstellung virtueller Maschinen, Ansible zur Konfigurationsverwaltung und Kubernetes zur Orchestrierung von Containern. Zusätzlich wird MicroK8s als leichtgewichtige Kubernetes-Distribution verwendet, um eine einfache und schnelle Einrichtung in einer lokalen Entwicklungsumgebung zu ermöglichen.

#### Dateien im Projekt

- **Vagrantfile**: Konfigurationsdatei für Vagrant zur Erstellung und Konfiguration virtueller Maschinen.

- **hosts**: Inventardatei für Ansible, die die zu konfigurierenden Hosts auflistet.

- **playbook.yml**: Ansible-Playbook zur Vorbereitung der Umgebung und Installation notwendiger Komponenten für Kubernetes und MicroK8s.

#### Voraussetzungen

- Vagrant
- Ansible
- VirtualBox oder eine andere VM-Lösung, die von Vagrant unterstützt wird

#### Anleitung

1. Repository klonen oder Dateien an einem gewünschten Ort entpacken.
2. Terminal öffnen und zum Verzeichnis mit den Dateien navigieren.
3. Virtuelle Maschinen mit Vagrant starten: `vagrant up
4. Das Ansible-Playbook wird bereits in Schritt 3 mit ausgeführt. Falls es wiederholt werden muss: `vagrant provision` oder `ansible-playbook -i hosts playbook.yml`
5. Auf den Server verbinden: `vagrant ssh`
6. Nach Abschluss der Konfiguration MicroK8s und Kubernetes-Befehle verwenden.

#### Wichtige Kubernetes-Befehle

- Cluster-Informationen anzeigen: `kubectl cluster-info`
- Pods in allen Namespaces auflisten: `kubectl get pods --all-namespaces`
- Deployment erstellen: `kubectl create deployment <name> --image=<image>`
- Deployments auflisten: `kubectl get deployments`
- Services auflisten: `kubectl get services`
- Deployment skalieren: `kubectl scale deployment <name> --replicas=<num>`
- Logs eines Pods anzeigen: `kubectl logs <pod-name>`
- Interaktive Shell in einem Pod starten: `kubectl exec -it <pod-name> -- /bin/bash`
- Pod löschen: `kubectl delete pod <pod-name>`
- Alle Ressourcen in einem Namespace löschen: `kubectl delete all --all --namespace=<namespace>`
- Kubernetes Dashboard starten: `kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0/aio/deploy/recommended.yaml` und Zugriff über: `kubectl proxy`
- Konfigurationsdatei eines Pods bearbeiten: `kubectl edit pod <pod-name>`
- Status der Nodes anzeigen: `kubectl get nodes`

#### Grundlegende MicroK8s-Befehle

MicroK8s bietet eine vereinfachte Befehlsstruktur, die mit dem Präfix `microk8s` beginnt, um Konflikte mit einer vorhandenen Kubernetes-Installation zu vermeiden.

- MicroK8s starten: `microk8s start`
- Alle MicroK8s-Services auflisten: `microk8s status --all`
- MicroK8s-Addons aktivieren: `microk8s enable <addon-name>`
- Pods auflisten: `microk8s kubectl get pods`
- Services auflisten: `microk8s kubectl get services`
- Deployment erstellen: `microk8s kubectl create deployment <name> --image=<image>`
- MicroK8s stoppen: `microk8s stop`
- MicroK8s Dashboard zugreifen: Zuerst das Dashboard-Addon aktivieren `microk8s enable dashboard`, dann Zugriff über: `microk8s dashboard-proxy`

